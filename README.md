# Insecure command runner

> RCEaaS

# Notice

**DO NOT RUN**

## License

Copyright 2024 Caleb Woodbine.
This project is licensed under the [AGPL-3.0](http://www.gnu.org/licenses/agpl-3.0.html) and is [Free Software](https://www.gnu.org/philosophy/free-sw.en.html).
This program comes with absolutely no warranty.
