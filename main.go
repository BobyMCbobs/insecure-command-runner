package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

const riskAcceptEnvName = "I_DECIDED_TO_RUN_THIS_AND_UNDERSTAND_THE_RISKS"
const defaultPort = "8083"

type rceHandler struct{}

func (rceHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		log.Printf("error parsing form: %s\n", err)
		return
	}
	command := r.Form.Get("command")
	var out strings.Builder
	cmd := exec.Command("sh", "-c", command)
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		log.Printf("error running command: %v\n", err)
		return
	}

	log.Printf("cmd: %v\noutput: %v\n", command, out.String())
	fmt.Fprint(w, out.String())
}

func main() {
	if val, ok := os.LookupEnv(riskAcceptEnvName); !(ok || val == "yes") {
		log.Printf("error: risk must be understood and env '%v' set to 'yes'\nexiting...\n", riskAcceptEnvName)
		return
	}
	mux := http.NewServeMux()
	mux.Handle("POST /run", rceHandler{})
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, `
<html>
  <head>
    <title>RCEaaS</title>
  </head>
  <body>
    <form action=/run method=post onsubmit="this.source.value=this.outerHTML">
      <label for="command">Command:</label>
      <input type="text" id="command" name="command"><br><br>
      <input type="hidden" name="source" />
      <input type="submit" value="Submit">
    </form>
  </body>
</html>
`)
	})
	port := defaultPort
	if val, ok := os.LookupEnv("PORT"); ok {
		port = val
	}
	log.Printf("Running on :%v\n", port)
	srv := &http.Server{
		Addr:    ":" + port,
		Handler: mux,
	}
	log.Fatal(srv.ListenAndServe())
}
